#ifndef MAPAS_H
#define MAPAS_H
#include <iostream>
#include <vector>
#include <windows.h>
#include <math.h>
#include "GL/glut.h"
#include "TextureManager.h"
using namespace std;
struct Pair{
    float pos_x;
    float pos_y;
};
class Mapas
{
    public:
        Mapas(){crear_mapa1();crear_mapa2();crear_mapa3();nivel = 0;}
        virtual ~Mapas(){}
        void crear_mapa1();
        void crear_mapa2();
        void crear_mapa3();
        void insertar_mapa();
        void dibujarcubo(int x , int y, char aux,bool bajo);
        void dibujar_mapa();
        void setTexture(int);
        void setPos(int x, int y,char a);
        void setMapa(vector<vector<char> > mapo){mapa[nivel] = mapo;}
        int nivel;
        vector<vector<vector<char> > >get_mapas();
    protected:
    private:
        GLint texture;
        vector<vector<vector<char> > > mapa;
};
void Mapas::setPos(int x, int y,char a)
{
    mapa[nivel][x][y] = a;
}
void Mapas::setTexture(int texture_)
{
    texture = texture_;
}
vector<vector<vector<char> > > Mapas ::get_mapas()
{
    return mapa;
}
void Mapas::crear_mapa1()
{
    vector<vector<char> > mapa1;
    mapa1 = {{'t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t'},
             {'t','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','t','l','l','l','l','l','t','a','t'},
             {'t','l','t','a','a','a','a','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','t','l','t'},
             {'t','l','t','a','a','a','a','l','l','l','l','l','l','t','t','t','t','t','t','t','t','t','l','l','l','l','l','t','l','t'},
             {'t','l','t','a','a','a','a','l','l','l','l','l','l','t','l','l','l','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','t','a','a','a','a','l','l','l','l','l','l','t','l','l','l','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','t','a','a','a','a','l','l','l','l','l','l','t','l','l','l','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','t','a','a','a','a','l','l','l','l','l','l','t','l','l','l','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','t','a','a','a','l','l','l','l','l','l','l','t','l','l','l','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','t','t','n','t','t','t','t','t','t','t','t','t','l','l','l','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','l','l','l','l','l','l','l','l','m','e','l','l','l','l','l','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','c','a','l','l','l','t','l','w','w','w','l','l','l','l','l','c','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','c','a','c','l','l','l','l','w','w','w','l','l','l','l','l','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','c','a','c','l','l','C','l','w','w','w','l','l','l','l','l','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','c','l','c','c','c','c','l','w','w','w','l','l','l','l','l','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','c','c','c','l','l','l','l','w','w','t','t','t','t','t','t','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','l','l','l','l','l','l','l','w','w','t','l','l','l','l','t','l','l','l','l','t','l','m','l','l','l','t','l','t'},
             {'t','l','l','l','l','l','l','l','l','l','l','t','l','l','c','c','n','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','l','l','l','l','l','l','l','l','l','t','l','l','l','l','t','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','t','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','l','l','l','l','l','l','l','l','l','t','l','l','l','l','t','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','l','l','l','l','l','l','l','l','l','t','l','l','l','l','t','l','l','l','l','t','l','l','l','l','l','l','l','t'},
             {'t','l','l','l','l','l','l','l','l','l','l','t','l','l','l','l','t','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','l','l','l','l','l','l','l','l','m','t','t','t','l','t','t','l','l','l','l','t','l','l','l','l','l','t','l','t'},
             {'t','l','l','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','l','l','l','l','l','t','a','t'},
             {'t','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','t','a','t'},
             {'t','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','t','a','t'},
             {'t','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','t','a','t'},
             {'t','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','t','a','t'},
             {'t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t'}};
    mapa.push_back(mapa1);
}
void Mapas::crear_mapa2()
{
    vector<vector<char> > mapa2;
    mapa2 = {{'n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n'},
             {'n','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','n','l','l','l','l','l','n','a','n'},
             {'n','l','n','a','a','a','a','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','n','l','n'},
             {'n','l','n','a','a','a','a','l','l','l','l','l','l','n','n','n','n','n','n','n','n','n','l','l','l','l','l','n','l','n'},
             {'n','l','n','a','a','a','a','l','l','l','l','l','l','n','l','l','l','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','n','a','a','a','a','l','l','l','l','l','l','n','l','l','l','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','n','a','a','a','a','l','l','l','l','l','l','n','l','l','l','l','l','l','l','n','l','m','l','l','l','n','l','n'},
             {'n','l','n','a','a','a','a','l','l','l','l','l','l','n','l','l','l','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','n','a','a','a','l','l','l','l','l','l','l','n','l','l','l','f','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','n','n','n','n','n','n','n','n','n','n','n','n','l','l','l','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','c','a','l','l','l','n','l','l','l','l','l','l','l','l','l','c','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','c','a','c','l','l','l','l','c','l','l','l','l','l','l','l','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','c','a','c','l','l','C','l','c','l','l','l','l','l','l','l','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','c','l','c','c','c','c','l','c','l','l','l','l','l','l','l','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','c','c','c','l','l','l','l','c','l','l','n','n','n','n','n','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','l','l','l','l','l','l','l','c','l','n','l','l','l','l','n','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','l','l','l','l','l','l','l','l','l','n','l','l','c','c','n','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','l','l','l','l','l','l','l','l','l','n','l','l','l','l','n','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','n','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','l','l','l','l','l','l','l','l','l','n','l','l','l','l','n','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','l','l','l','l','l','l','l','l','l','n','l','l','l','l','n','l','l','l','l','n','l','l','l','l','l','l','l','n'},
             {'n','l','l','l','l','l','l','l','l','l','l','n','l','l','l','l','n','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','l','l','l','l','l','l','l','l','l','n','n','n','l','n','n','l','l','l','l','n','l','l','l','l','l','n','l','n'},
             {'n','l','l','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','l','l','l','l','l','n','a','n'},
             {'n','l','l','l','l','m','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','n','a','n'},
             {'n','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','m','l','l','l','l','n','a','n'},
             {'n','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','n','a','n'},
             {'n','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','l','n','a','n'},
             {'n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n'}};
    mapa.push_back(mapa2);
}
void Mapas::crear_mapa3()
{
    vector<vector<char> > mapa3;
    mapa3 = {{'t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t'},
             {'t','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','t','s','s','s','s','s','t','a','t'},
             {'t','s','t','a','a','a','a','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','t','s','t'},
             {'t','s','t','a','a','a','a','s','s','s','s','s','s','t','t','t','t','t','t','t','t','t','s','s','s','s','s','t','s','t'},
             {'t','s','t','a','a','a','a','s','s','s','s','s','s','t','s','s','s','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','t','a','a','a','a','s','s','s','s','s','s','t','s','s','s','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','t','a','a','a','a','s','s','s','s','s','s','t','s','s','s','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','t','a','a','a','a','s','s','s','s','s','s','t','s','s','s','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','t','a','a','a','s','s','s','s','s','s','s','t','s','s','s','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','t','t','n','t','t','t','t','t','t','t','t','t','s','s','s','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','b','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','c','a','s','s','s','t','s','w','w','w','s','b','s','s','s','c','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','c','a','c','s','s','s','s','w','w','w','s','s','e','s','s','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','c','a','c','s','s','C','s','w','w','w','s','s','s','s','s','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','c','s','c','c','c','c','s','w','w','w','b','s','s','s','s','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','c','c','c','s','s','s','s','w','w','t','t','t','t','t','t','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','s','s','s','s','s','s','s','w','w','t','s','s','s','s','t','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','s','s','s','s','s','s','s','s','s','t','s','s','c','c','n','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','s','s','s','s','s','s','s','s','s','t','s','s','s','s','t','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','t','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','s','s','s','s','s','s','s','s','s','t','s','s','s','s','t','s','s','s','s','t','b','s','s','s','s','t','s','t'},
             {'t','s','s','s','s','s','s','s','s','s','s','t','s','s','s','s','t','s','s','s','s','t','s','s','s','s','s','s','s','t'},
             {'t','s','s','s','s','s','s','s','s','s','s','t','s','s','s','s','t','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','s','s','s','s','s','s','s','s','s','t','t','t','s','t','t','s','s','s','s','t','s','s','s','s','s','t','s','t'},
             {'t','s','s','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','s','s','s','s','s','t','a','t'},
             {'t','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','t','a','t'},
             {'t','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','t','a','t'},
             {'t','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','t','a','t'},
             {'t','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','s','t','a','t'},
             {'t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t','t'}};
    mapa.push_back(mapa3);
}
void Mapas::dibujarcubo(int x, int y, char aux,bool bajo)
{
    double x_ =0.0625f;
    double y_ =0.125f;
    int x_pared = 1;
    int y_pared = 7;
    if(aux == 'l'){//espacio libre
        x_pared = 4;
        y_pared = 5;
    }else if(aux == 'c'){//caja simple
        x_pared = 14;
        y_pared = 5;
    }
    else if(aux == 'a'){//adorno libre
        x_pared = 5;
        y_pared = 4;
    }else if(aux == 'n'){//caja negra
        x_pared = 13;
        y_pared = 5;
    }
    else if(aux == 'w'){//agua
        x_pared = 13;
        y_pared = 4;
    }
    else if(aux == 's'){//tierra suelo
        x_pared = 8;
        y_pared = 3;
    }
    else if(aux == 'e'){//tierra escalera
        x_pared = 0;
        y_pared = 5;
    }
    else if(aux == 'f'){//tierra escalera
        x_pared = 1;
        y_pared = 5;
    }
    else if(aux == 'm'){//maletin
        x_pared = 5;
        y_pared = 5;
    }
    else if(aux == 'b'){//maletin
        x_pared = 7;
        y_pared = 3;
    }
    else if(aux == 'k'){//llave
        x_pared = 6;
        y_pared = 5;
    }
    else if(aux == 'i'){//llave de tierra
        x_pared = 6;
        y_pared = 3;
    }
    if(bajo){

        glBegin(GL_POLYGON);
        glTexCoord2f(x_pared*x_,y_pared*y_);//coordenadas de textura
        glVertex3d(x, 0,y);
        //16 -12
        glTexCoord2f(x_pared*x_,(y_pared+1)*y_);
        glVertex3d(x,0,y+4);

        glTexCoord2f((x_pared+1)*x_,(y_pared+1)*y_);
        glVertex3d(x+4, 0,y+4);

        glTexCoord2f((x_pared+1)*x_,y_pared*y_);
        glVertex3d(x+4,0,y);
        glEnd();
    }
    else if(!bajo){
        glBegin(GL_POLYGON);
        glTexCoord2f(x_pared*x_,y_pared*y_);//coordenadas de textura
        glVertex3d(x,4,y);
        //16 -12
        glTexCoord2f(x_pared*x_,(y_pared+1)*y_);
        glVertex3d(x,4,y+4);

        glTexCoord2f((x_pared+1)*x_,(y_pared+1)*y_);
        glVertex3d(x+4,4,y+4);

        glTexCoord2f((x_pared+1)*x_,y_pared*y_);
        glVertex3d(x+4,4,y);
        glEnd();
        //atras
         glBegin(GL_POLYGON);
        glTexCoord2f(x_pared*x_,y_pared*y_);//coordenadas de textura
        glVertex3d(x,4, y+4);
        //16 -12
        glTexCoord2f(x_pared*x_,(y_pared+1)*y_);
        glVertex3d(x,0,y+4);

        glTexCoord2f((x_pared+1)*x_,(y_pared+1)*y_);
        glVertex3d(x+4,0,y+4);

        glTexCoord2f((x_pared+1)*x_,y_pared*y_);
        glVertex3d(x+4,4,y+4);

        glEnd();

        //FRENTE
        glBegin(GL_POLYGON);
        glTexCoord2f(x_pared*x_,y_pared*y_);//coordenadas de textura
        glVertex3d(x+4,4,y+4);
        //16 -12
        glTexCoord2f(x_pared*x_,(y_pared+1)*y_);
        glVertex3d(x+4,0,y+4);

        glTexCoord2f((x_pared+1)*x_,(y_pared+1)*y_);
        glVertex3d(x+4,0,y);

        glTexCoord2f((x_pared+1)*x_,y_pared*y_);
        glVertex3d(x+4,4,y);
        glEnd();

        //DERECHA

        glBegin(GL_POLYGON);
        glTexCoord2f(x_pared*x_,y_pared*y_);//coordenadas de textura
        glVertex3d(x,4, y);
        //16 -12
        glTexCoord2f((x_pared+1)*x_,y_pared*y_);
        glVertex3d(x+4,4,y);

        glTexCoord2f((x_pared+1)*x_,(y_pared+1)*y_);
        glVertex3d(x+4,0,y);

        glTexCoord2f(x_pared*x_,(y_pared+1)*y_);
        glVertex3d(x,0,y);
        glEnd();

        //izquierda
        glBindTexture(GL_TEXTURE_2D, texture);
        glBegin(GL_QUADS);
        glTexCoord2f(x_pared*x_,y_pared*y_);//coordenadas de textura
        glVertex3d(x,0,y);
        //16 -12
        glTexCoord2f((x_pared+1)*x_,y_pared*y_);
        glVertex3d(x,0,y+4);

        glTexCoord2f((x_pared+1)*x_,(y_pared+1)*y_);
        glVertex3d(x,4,y+4);

        glTexCoord2f(x_pared*x_,(y_pared+1)*y_);
        glVertex3d(x,4,y);
        glEnd();
   }
}
void Mapas::dibujar_mapa()
{
    bool bajo = 0;
    for(int i = 0 ; i < mapa[nivel].size();++i){
        for(int j = 0 ; j < mapa[nivel][i].size();++j)
        {
            if(mapa[nivel][i][j] == 'l' || mapa[nivel][i][j] == 'a' || mapa[nivel][i][j] == 's'|| mapa[nivel][i][j] == 'w' )
                bajo = 1;
            dibujarcubo(i*4-30,j*4 -30,mapa[nivel][i][j],bajo);
            bajo = 0;
        }
    }

}
#endif // MAPAS_H
