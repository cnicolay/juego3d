#include "Mapas.h"
#include "Music.h"
#include <cstdio>
#include <time.h>
#include <iostream>
using namespace std;

#define izquierda 0
#define derecha 1
#define arriba 2
#define abajo 3

//clase enemigo
class Enemigo
{
    public:
        Enemigo();
        void recorrer();
        void revisar();
        bool preguntar(float px, float py);
        bool is_libre(float px, float py);
        void dibujar_enemigo(float ancho,float alto);
        void set_inicio(float pos_x_,float pos_y_){inicio.pos_x = pos_x_; inicio.pos_y = pos_y_;}
        void set_sprite(int sprite_){sprites = sprite_;}
        void set_pos_heroe(float pos_x_,float pos_y_){pos_heroe.pos_x = pos_x_;pos_heroe.pos_y = pos_y_;}
        bool getAlerta(){return alerta;}
        void set_mapas(Mapas m){mm = m;}
        bool get_muerto(){return muerto;}
        Mapas get_mapa(){return mm;}
        Pair get_inicio(){return inicio;}
        void muerte();
        void reiniciar();
        bool muerto;
        bool heroe_muerto;
        float salud_heroe;
        bool caja;
    private:
        vector<int> ronda;
        Pair inicio;
        int orientacion;
        bool alerta;
        int sprites;
        Mapas mm;
        float pos_sprite;
        Pair pos_heroe;
        float dt;
        float rango;
        int c;
};

Enemigo::Enemigo()
{


    caja = 0;
    heroe_muerto = 0;
    rango = 20;
    dt = 0.06;
    pos_sprite = 0 ;

    //srand(time(NULL));
    inicio.pos_x = rand()%70;
    inicio.pos_y = rand()%70;
    while(!is_libre(inicio.pos_x,inicio.pos_y)){
        inicio.pos_x = rand()%70;
        inicio.pos_y = rand()%70;
    }
    orientacion = abajo;
    alerta = 0;
}
void Enemigo:: muerte()
{
    muerto = 1;
    int x_=(inicio.pos_x+30)/4;
    int y_=(inicio.pos_y+30)/4;
    if(mm.nivel == 0 || mm.nivel == 1)
        mm.setPos(x_,y_,'k');
    else
        mm.setPos(x_,y_,'i');
}
void Enemigo::reiniciar()
{
    heroe_muerto = 0;
    muerto = 0;
    rango = 5;//10;
    dt = 0.5;//0.06;
    pos_sprite = 0 ;
    inicio.pos_x = rand()%70;
    inicio.pos_y = rand()%70;
    while(!is_libre(inicio.pos_x,inicio.pos_y)){
        inicio.pos_x = rand()%70;
        inicio.pos_y = rand()%70;
    }

    orientacion = abajo;
    alerta = 0;
}
void Enemigo::revisar()
{
    alerta = 0;
}
int diferencia(float a, float b)
{
    if(a < 0 && b <= 0 )
        if(a> b) return a - b; else return -a + b;
    else if (a <0 && b > 0)
        return b -a;
    else if(a > 0 && b < 0)
        return a-b;
    else
        if(a> b) return a -b; else return b -a;
}
void Enemigo::recorrer()
{
    //ai
    if(alerta)
    {
        float max_ab = diferencia(pos_heroe.pos_x, inicio.pos_x);
        float max_id = diferencia(pos_heroe.pos_y, inicio.pos_y);
        if(max_ab > max_id){
            if(pos_heroe.pos_x < inicio.pos_x )
               orientacion = izquierda;
            else if(pos_heroe.pos_x > inicio.pos_x)
               orientacion = derecha;
        }
        else{
            if(pos_heroe.pos_y > inicio.pos_y)
               orientacion = arriba;
            else if(pos_heroe.pos_y < inicio.pos_y)
               orientacion = abajo;
        }
    }
    //logica
    if(orientacion == izquierda)
    {
        if(is_libre(inicio.pos_x-dt,inicio.pos_y)){
            inicio.pos_x = inicio.pos_x -dt;
       }else  orientacion = rand()%4;
    }else if(orientacion == derecha)
    {
        if(is_libre(inicio.pos_x+dt,inicio.pos_y)){
            inicio.pos_x = inicio.pos_x +dt;
        }else  orientacion = rand()%4;
    }else if(orientacion == arriba)
    {
        if(is_libre(inicio.pos_x,inicio.pos_y+dt)){
            inicio.pos_y = inicio.pos_y +dt;
        }else  orientacion = rand()%4;
    }else if(orientacion == abajo)
    {
        if(is_libre(inicio.pos_x,inicio.pos_y-dt)){
            inicio.pos_y = inicio.pos_y -dt;
        }else  orientacion = rand()%4;
    }

}
void Enemigo::dibujar_enemigo(float ancho,float alto)
{
    glPushMatrix();
        recorrer();
        glTranslatef(inicio.pos_x,1,inicio.pos_y);
        glColor3f(1,1,0);
        //glTranslated(0,1,0);
        glutSolidSphere(1,20,20);
        glColor3f(1,0,1);
        glutSolidCube(1.5);
        glRotatef(90,1,0,0);
        glutSolidTorus(0.1,1.5,20,20);
        glColor3f(1,1,1);
    glPopMatrix();
}
bool Enemigo::preguntar(float px, float py)
{

    //if(pos_heroe.pos_x -rango <=px && pos_heroe.pos_x +rango >=px && pos_heroe.pos_y -rango <= py &&pos_heroe.pos_y +rango >= py)
    if(pos_heroe.pos_x<=px +rango && pos_heroe.pos_x  >=px -rango&& pos_heroe.pos_y  <= py +rango&&pos_heroe.pos_y >= py-rango && !caja)
    {
        alerta = 1;
        dt = dt+ 0.0001;

    }
    else {
        dt = 0.06;
        alerta = 0;
    }

    if(pos_heroe.pos_x -1.5 <=px && pos_heroe.pos_x +1.5 >=px && pos_heroe.pos_y -1.5 <= py &&pos_heroe.pos_y +1.5 >= py){
        salud_heroe = salud_heroe - 0.1;
        if(salud_heroe <= 0)
            heroe_muerto = 1;
    }
    int x_=(px+30)/4;
    int y_ =(py+30)/4;
    char a =mm.get_mapas()[mm.nivel][x_][y_];
    if(a == 'l' || a == 'a' || a == 's'|| a == 'm' || a == 'b' || a == 'w' ||a== 'e')
       return 1;
    return 0;
}
bool Enemigo::is_libre(float px, float py)
{
    if(preguntar(px-1.5,py-1.5) && preguntar(px+1.5,py-1.5)&&preguntar(px+1.5,py+1.5) && preguntar(px-1.5,py+1.5))
        return true;
    return false;
}
