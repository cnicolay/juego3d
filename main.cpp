#define GLUT_DISABLE_ATEXIT_HACK
#include <windows.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "TextureManager.h"
#include "GL/glut.h"
using namespace std;

#define RED 0
#define GREEN 0
#define BLUE 0
#define ALPHA 1
#define GL_BGR 0x80E0
#define GL_BGRA 0x80E1
#define ECHAP 27
#include "Mapas.h"
#include "enemigo.h"
#include "Music.h"
GLint texture;
GLint sprites_enemigo;
Mapas mm;
Music musica;
Enemigo * enemigos = new Enemigo[20];

void init_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid window_key(unsigned char key, int x, int y);

GLfloat t_d;
GLfloat t_a;
GLfloat pos_x = 0;
GLfloat pos_z = 0;
bool contacto = 0;
GLfloat Light0Pos[] = {pos_x, 30.0f, pos_z, 1.0f};

//variables para el gizmo
float delta_x = 0.0;
float delta_y = 0.0;
float mouse_x = 400;
float mouse_y;
float var_x = 0.0;
float var_z = 0.0;
float step = 0; //0.0 Posicion inicial. 1.0 Traslacion. 2.0 Primera Rotacion(en y). 3.0 Segunda Rotacion (en x) 4.0 Ultima Rotacion (en z)
bool maletin = 0 ;
bool llave = 0;
GLvoid callback_special(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		var_z = 1;
		glutPostRedisplay();
		break;

	case GLUT_KEY_DOWN:
		var_z = -1;
		glutPostRedisplay();
		break;

	case GLUT_KEY_LEFT:
	    var_x = 1;
		glutPostRedisplay();
		break;

	case GLUT_KEY_RIGHT:
		var_x = -1;
		glutPostRedisplay();
		break;

	case GLUT_KEY_PAGE_UP:
		step++;
		glutPostRedisplay();
		break;
	case GLUT_KEY_PAGE_DOWN:
		step--;
		glutPostRedisplay();
		break;
	}
}

GLvoid callback_special_up(int key, int x, int y)
{
    switch (key)
	{
        case GLUT_KEY_UP:
            var_z = 0;
            break;
        case GLUT_KEY_DOWN:
            var_z = 0;
            break;
        case GLUT_KEY_LEFT:
            var_x = 0;
            break;
        case GLUT_KEY_RIGHT:
            var_x = 0;
            break;
	}
}

GLvoid callback_mouse(int button, int state, int x, int y)
{
     if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
        musica.sonido_disparo();
    }
    if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
        musica.sonido_disparo();
    if(button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN)
        musica.cambiar_musica();


}
GLvoid callback_motion(int x, int y)
{
    if(1)
    {
        if(delta_x > 360 || delta_x < -360)
            delta_x = 0;
        delta_x += x - mouse_x;
        //mouse_x = x;
        //delta_y += y - mouse_y;
        //mouse_y = y;
        glutPostRedisplay();
    }
}

//function called on each frame
GLvoid window_idle();

int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(800, 800);
	glutInitWindowPosition(150, 0);
	glutCreateWindow("Juego 3d");

	initGL();
	glutSetCursor(GLUT_CURSOR_CROSSHAIR);
    texture = TextureManager::Inst()->LoadTexture("imagenes/fondo.png", GL_BGRA, GL_RGB);//GL_BGR_EXT
   	sprites_enemigo = TextureManager::Inst()->LoadTexture("imagenes/enemigoj.png", GL_BGRA, GL_RGBA);//GL_RGBA_EXT
	glutDisplayFunc(&window_display);
	glutReshapeFunc(&window_reshape);

	glutMouseFunc(&callback_mouse);
	glutPassiveMotionFunc(&callback_motion);
	glutKeyboardFunc(&window_key);
	//glutKeyboardUpFunc(&window_key_up); //key release events
	glutSpecialFunc(&callback_special);
	glutSpecialUpFunc(&callback_special_up);

	//function called on each frame
	glutIdleFunc(&window_idle);

	glutMainLoop();

	return 1;
}


GLvoid initGL()
{

    glShadeModel(GL_SMOOTH); // modelo de shading try GL_FLAT
    glEnable(GL_CULL_FACE); //no trata las caras escondidas
    glEnable(GL_DEPTH_TEST); // Activa el Z-Buffer
    glDepthFunc(GL_LEQUAL); //Modo de funcionamiento del Z-Buffer
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_DEPTH_TEST);
    musica.play_music();
	///iluminacion
	GLfloat Light0Amb[4] = {0.0f, 0.0f, 0.0f, 1.0f};
    GLfloat Light0Dif[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat Light0Spec[4]= {1.0f, 1.0f, 1.0f, 1.0f};
    // Valores de posici�n : luz puntual

    glLightfv(GL_LIGHT0, GL_AMBIENT, Light0Amb);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, Light0Dif);
    glLightfv(GL_LIGHT0, GL_SPECULAR, Light0Spec);
    // la posici�n de la luz 0
    glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
    // Activaci�n de la luz
    glEnable(GL_LIGHTING);
    //Activaci�n de la luz 0
    glEnable(GL_LIGHT0);

	glEnable(GL_TEXTURE_2D);
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glClearColor(RED, GREEN, BLUE, ALPHA);
}
void funcion_juego()
{
    mm.setTexture(texture);
    mm.dibujar_mapa();
    for(int i =0 ; i < 20  ;++i)
    {
        if(!enemigos[i].muerto){
            enemigos[i].set_mapas(mm);
            enemigos[i].set_sprite(sprites_enemigo);
            enemigos[i].dibujar_enemigo(3,3);
            enemigos[i].set_pos_heroe(pos_x,pos_z);
        }
        else {
            mm.setMapa(enemigos[i].get_mapa().get_mapas()[mm.nivel]);
        }
    }
}
bool preguntar(float pos_x,float pos_y)
{
    int x_=(pos_x+30)/4;
    int y_ =(pos_z+30)/4;

    char a =mm.get_mapas()[mm.nivel][x_][y_];
    if(a == 'l' || a == 'a' || a == 's'){
       return 1;

    }
    if(a == 'e'){
        if(maletin == 1 && llave == 1){
            for(int i = 0 ; i< 20 ;++i){
                 enemigos[i].reiniciar();
            }
            mm.nivel=1;
            maletin = 0;
            llave = 0;
        }
       return 1;
    }
    if(a == 'f'){
        if(maletin == 1 && llave == 1){
            for(int i = 0 ; i< 20 ;++i){
                 enemigos[i].reiniciar();
            }

            mm.nivel=2;
            maletin = 0;
            llave = 0;

        }
       return 1;
    }
    if(a == 'm' && (mm.nivel == 0 || mm.nivel == 1)){
        musica.sonido_objeto();
        maletin = 1;
        mm.setPos(x_,y_,'l');
        return 1;
    }
    if(a == 'b' && mm.nivel == 2){
        musica.sonido_objeto();
        maletin = 1;
        mm.setPos(x_,y_,'s');
        return 1;
    }
    if(a == 'k' && (mm.nivel == 0 || mm.nivel == 1)){
        musica.sonido_objeto();
        llave = 1;
        mm.setPos(x_,y_,'l');
        return 1;
    }
    if(a == 'i' && mm.nivel == 2){
        musica.sonido_objeto();
        llave = 1;
        mm.setPos(x_,y_,'s');
        return 1;
    }
    return 0;
}

bool is_libre(float pos_x,float pos_y)
{
    if(preguntar(pos_x-1.5,pos_y-1.5) && preguntar(pos_x+1.5,pos_y-1.5) && preguntar(pos_x+1.5,pos_y+1.5) && preguntar(pos_x-1.5,pos_y+1.5))
    {
        contacto =1;
        return true;
    }
    else
    contacto = 0;
    return false;
}
GLvoid window_display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);//funcion de transparencia
	glEnable(GL_BLEND);//utilizar transparencia

    t_d = glutGet(GLUT_ELAPSED_TIME) / 1000.0 - t_a;

    if(!is_libre(pos_x +var_x * t_d * 10,pos_z + var_z * t_d * 10))
    {
        var_x = 0;
        var_z = 0;
    }
    if(var_x || var_z)
    {
        pos_x += var_x * t_d * 10;
        pos_z += var_z * t_d * 10;

    }

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	Light0Pos[0] = pos_x;
	Light0Pos[2] = pos_z;
    glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);

	static float t=0.7;
    t+=t_d;
    if(var_x || var_z){
        if(t>0.3){
            musica.sonido_caminar();
            t=0;
        }
    }
	gluPerspective(60.0f, 1.0f, 0.01f, 1000.0f);


	glRotatef(delta_x, 0.0, 1.0, 0.0);
	glTranslatef(pos_x, -2.0, pos_z);
	//gluLookAt(-5 + pos_x, 2, pos_z, pos_x, 2, pos_z, 0, 1, 0);



    //aqui juego
    funcion_juego();

    t_a = glutGet(GLUT_ELAPSED_TIME) / 1000.0;

    glutWarpPointer(400, 400);
	glutSwapBuffers();

	glFlush();
}

GLvoid window_reshape(GLsizei width, GLsizei height)
{
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
}

GLvoid window_key(unsigned char key, int x, int y)
{
	switch (key) {
	case ECHAP:
		exit(1);
		break;
	default:
		printf("tecla %d activa.\n", key);
		break;
	}
}


//function called on each frame
GLvoid window_idle()
{
	glutPostRedisplay();
}
